# CapturyOSC WPF
## Ein WPF-Tool zum Senden und Empfangen von OSC-Daten für Captury

###### Quickstart Empfangen
Um das Tool nutzen zu können muss die .exe Datei aus dem bin/Debug Ordner gestartet werden. Es wird lediglich die .exe benötigt und kann an jeden beliebigen Ort kopiert werden.
Nach dem Start muss eine IP-Adresse und Port von Captury angegeben werden, an der die Subscribe-Messages mit den ausgewählten Joints geschickt werden soll.
Dann muss man die IP-Adresse und Port zum Empfangen der Daten von OSC angeben. (Die IP des Computers an dem CapturyOSC WPF gestartet wurde und der Port über den die OSC-Daten empfangen werden sollen)
Es sollte ein Dateiname festgelegt werden und optional kann ein Ordner zur Speicherung der OSC-Datei angegeben werden. (Standardmäßig wird die Datei in %APPDATA%/CapturyOSC gespeichert)
Sind alle Felder ausgefüllt, muss nur noch Start geklickt werden und es werden Subscribe-Nachrichten an Captury geschickt und Captury sendet die OSC-Daten für die einzelnen Joints zurück.
In der Konsole sieht man die Daten welches das Tool empfängt. (Man kann die Konsole auch ausblenden)
Das Programm schreibt alle empfangenen Daten in eine .osc Datei und speichert diese ab.
Mit dem Stop-Knopf beendet man das Empfangen der Daten.

###### Quickstart Senden
Damit man die empfangenen Daten von Captury an VizArtist oder ähnliches schicken kann, muss nur die IP und der Port des Gerätes angegeben werden, welches die OSC-Daten empfangen soll.
Danach muss nur noch die Datei ausgewählt werden, welche gesendet werden soll.
Anschließend klickt man auf Start und die Daten werden in einer Endlosschleife gesendet.
Mit dem Stop-Knopf beendet man das Senden der Daten.

## TODOs
- Status-LED hinzufügen
- Presets zum Speichern von Konfigurationen
- Menü zum Laden und Speichern von Presets
- Performance testen/anpassen
- Refactoring
