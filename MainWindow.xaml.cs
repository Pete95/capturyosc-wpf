﻿using System;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Diagnostics;
using System.Timers;
using SharpOSC;
using WinForms = System.Windows.Forms;
using System.ComponentModel;
using System.Globalization;

namespace CapturyOSCWPF
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        static string directoryPath;
        static IPEndPoint remoteEP;
        static UdpClient udpClient;
        static string fileName;
        static StreamWriter streamWriter;
        static Boolean killUDPCallback;
        static Timer timer;
        static String sPath;
        static String sFile;
        static Boolean toggled;
        static Boolean started = false;
        static bool sendData;
        static int peopleCounter;
        static string[][] subscribeMessageArray;
        static string[] jointNames;
        static Stopwatch swTimestamp;

        public MainWindow()
        {
            InitializeComponent();
            peopleCounter = 1;
        }

        private void startBtn_Click(object sender, RoutedEventArgs e)
        {
            if (!started)
            {
                swTimestamp = new Stopwatch();
                subscribeMessageArray = new string[peopleCounter][];
                killUDPCallback = false;
                string ipSubMessage = this.ipSubMessage.Text;
                int portSubMessage = Int32.Parse(this.portSubMessage.Text);
                if (!String.IsNullOrEmpty(sPath))
                {
                    directoryPath = sPath;
                }
                else
                {
                    directoryPath = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "CapturyOSC");
                }

                fileName = this.oscFileName.Text + ".osc";
                remoteEP = new IPEndPoint(IPAddress.Parse(ipSubMessage), portSubMessage);
                //create file directory
                if (!Directory.Exists(directoryPath))
                {
                    Directory.CreateDirectory(directoryPath);
                }

                //open streamwriter to write data to file
                streamWriter = File.CreateText(System.IO.Path.Combine(directoryPath, fileName));

                //init sender
                try
                {
                    string ipSender = this.ipReceiveData.Text;
                    int portSender = Int32.Parse(this.portReceiveData.Text);
                    udpClient = new UdpClient(new IPEndPoint(IPAddress.Parse(ipSender), portSender));
                    udpClient.Connect(remoteEP);
                    //send starting message
                    jointNames = subscribeContent.Text.Split(',');
                    string[] subscribe;

                    for(int i = 1; i <= peopleCounter; i++)
                    {
                        if(i == 1)
                        {
                            subscribe = subscribeMessage.Text.Split('+');
                            subscribeMessageArray[0] = subscribe;
                            
                        }
                        else
                        {
                            TextBox tmp = (TextBox)this.subscribeMessageGrid.FindName("subscribeMessage" + i);
                            subscribe = tmp.Text.Split('+');
                            subscribeMessageArray[i-1] = subscribe;
                        }
                        foreach (string joint in jointNames)
                        {
                            var message = new OscMessage(subscribe[0] + joint + subscribe[2]);
                            udpClient.Send(message.GetBytes(), message.GetBytes().Length);
                        }
                    }
                    

                    udpClient.BeginReceive(new AsyncCallback(callback), udpClient);

                    //setup timer and subscription messages
                    timer = new Timer();
                    timer.Interval = Int32.Parse(this.timerInterval.Text);
                    timer.Enabled = true;

                    Stopwatch sw = Stopwatch.StartNew();
                    long start = 0;
                    long end = sw.ElapsedMilliseconds;

                    if (killUDPCallback == false)
                    {
                        timer.Elapsed += (o, E) =>
                        {
                            start = end;
                            end = sw.ElapsedMilliseconds;
                            for (int i = 0; i < peopleCounter; i++)
                            {
                                subscribe = subscribeMessageArray[i];
                                foreach (string joint in jointNames)
                                {
                                    var message = new OscMessage(subscribe[0] + joint + subscribe[2]);
                                    udpClient.Send(message.GetBytes(), message.GetBytes().Length);
                                }
                            }
                        };
                    }

                    WriteLine("Started");
                    started = true;
                }
                catch (Exception E)
                {
                    WriteLine(E.Message);
                    started = false;
                    streamWriter.Close();
                }
            }
            else
            {
                WriteLine("Already Started");
            }
        }

        private void ReadAndWriteOscMessage(StreamWriter streamWriter, OscMessage receivedPacket)
        {
            if(!swTimestamp.IsRunning)
            {
                swTimestamp = Stopwatch.StartNew();
            }
            streamWriter.Write((int) swTimestamp.Elapsed.TotalMilliseconds + ";");
            streamWriter.Write(receivedPacket.Address);
            foreach (object argument in receivedPacket.Arguments)
            {
                streamWriter.Write(";");
                streamWriter.Write(argument.ToString() + "|f");
            }
            streamWriter.Write("\n");
        }

        private void oscMessageConsole(OscMessage receivedPacket)
        {
            String receivedLine = "";
            BackgroundWorker worker = new BackgroundWorker();
            worker.WorkerReportsProgress = true;
            worker.DoWork += delegate (object sender, DoWorkEventArgs e) {
                receivedLine = receivedPacket.Address;
                foreach (object argument in receivedPacket.Arguments)
                {;
                    receivedLine += argument.ToString();
                }
                (sender as BackgroundWorker).ReportProgress(1, receivedLine);
            };
            worker.ProgressChanged += delegate (object sender, ProgressChangedEventArgs e) {
                WriteLine((String)e.UserState);
            };
            worker.RunWorkerAsync(10000);
        }

        //The callback function
        private void callback(IAsyncResult ar)
        {

            if (killUDPCallback == false)
            {
                UdpClient udpClient = ((UdpClient)(ar.AsyncState));
                byte[] receiveBytes = udpClient.EndReceive(ar, ref remoteEP);
                string receiveString = Encoding.ASCII.GetString(receiveBytes);
                OscPacket packet = OscPacket.GetPacket(receiveBytes);
                OscBundle receivedBundle = packet as OscBundle;

                if (receivedBundle == null)
                {
                    OscMessage receivedMessage = packet as OscMessage;
                    ReadAndWriteOscMessage(streamWriter, receivedMessage);
                }
                else
                {
                    foreach (OscMessage receivedMessage in receivedBundle.Messages)
                    {
                        ReadAndWriteOscMessage(streamWriter, receivedMessage);
                        oscMessageConsole(receivedMessage);
                    }
                }
                udpClient.BeginReceive(new AsyncCallback(callback), udpClient);
               
            }

        }

        private void stopBtn_Click(object sender, RoutedEventArgs e)
        {
            if (started)
            {
                killUDPCallback = true;
                started = false;               
                if(udpClient != null && timer != null)
                {
                    udpClient.Close();
                    timer.Stop();
                }
                
                streamWriter.Close();
                swTimestamp.Stop();
                WriteLine("Stopped");
            }
            else
            {
                WriteLine("Already Stopped");
            }
        }

        private void pathButton_Click(object sender, RoutedEventArgs e)
        {
            WinForms.FolderBrowserDialog folderDialog = new WinForms.FolderBrowserDialog();
            folderDialog.ShowNewFolderButton = false;
            folderDialog.SelectedPath = System.AppDomain.CurrentDomain.BaseDirectory;
            WinForms.DialogResult result = folderDialog.ShowDialog();

            if (result == WinForms.DialogResult.OK)
            {
                sPath = folderDialog.SelectedPath;
                TextBlock tb = this.selectedFolderInfo;
                tb.Text = "";
                tb.Inlines.Add("Ausgewählter Ordner: ");
                tb.Inlines.Add(new Run(sPath) { FontWeight = FontWeights.Bold });
            }
            
        }

        private void sendComboItem_Selected(object sender, RoutedEventArgs e)
        {
            this.sendStartStop.Visibility = Visibility.Visible;
            this.timeIntervalGrid.Visibility = Visibility.Collapsed;
            this.receiveStartStop.Visibility = Visibility.Collapsed;
            this.receiverText.Text = "IP und Port für Empfänger der OSC-Daten";
            this.senderIPText.Visibility = Visibility.Collapsed;
            this.senderIPGrid.Visibility = Visibility.Collapsed;
            this.pathText.Visibility = Visibility.Collapsed;
            this.fileButton.Visibility = Visibility.Visible;
            this.folderPath.Visibility = Visibility.Collapsed;
            this.selectedFolderInfo.Text = "Keine Datei ausgewählt";
            this.subscribeMessageGrid.Visibility = Visibility.Collapsed;
            this.subscribeContent.Visibility = Visibility.Collapsed;
            this.subscribeMessageHeadline.Visibility = Visibility.Collapsed;
            this.subscribeContentHeadline.Visibility = Visibility.Collapsed;
            this.consoleText.Visibility = Visibility.Collapsed;
            this.consoleBox.Visibility = Visibility.Collapsed;
            this.peopleCount.Visibility = Visibility.Collapsed;
            this.mainWindow.Height = 300;
            toggled = true;
        }

        private void receiveComboItem_Selected(object sender, RoutedEventArgs e)
        {
            if(toggled == true)
            {
                this.timeIntervalGrid.Visibility = Visibility.Collapsed;
                this.sendStartStop.Visibility = Visibility.Collapsed;
                this.receiveStartStop.Visibility = Visibility.Visible;
                this.receiverText.Text = "Empfänger der Subscribe-Nachricht";
                this.senderIPText.Visibility = Visibility.Visible;
                this.senderIPGrid.Visibility = Visibility.Visible;
                this.pathText.Visibility = Visibility.Visible;
                this.fileButton.Visibility = Visibility.Collapsed;
                this.folderPath.Visibility = Visibility.Visible;
                this.subscribeMessageGrid.Visibility = Visibility.Visible;
                this.subscribeContent.Visibility = Visibility.Visible;
                this.subscribeMessageHeadline.Visibility = Visibility.Visible;
                this.subscribeContentHeadline.Visibility = Visibility.Visible;
                this.consoleText.Visibility = Visibility.Visible;
                this.consoleBox.Visibility = Visibility.Visible;
                this.peopleCount.Visibility = Visibility.Visible;
                this.mainWindow.Height = 920;

                TextBlock tb = this.selectedFolderInfo;
                tb.Text = "";
                tb.Inlines.Add("Kein Ordner ausgewählt! Der Standardpfad ist: ");
                tb.Inlines.Add(new Run("%APPDATA%/CapturyOSC") { FontWeight = FontWeights.Bold });
                toggled = false;
            }
        }

        private void startBtnSend_Click(object sender, RoutedEventArgs e)
        {
            sendData = true;
            sendOscDataFromFile();
        }

        void sendOscDataFromFile()
        {
            Stopwatch sw = Stopwatch.StartNew();
            int people = Int32.Parse(peopleNumber.Text);
            double timing;

            timing = 1;

            try
            {
                string ipSender = this.ipSubMessage.Text;
                int portSender = Int32.Parse(this.portSubMessage.Text);
                IPEndPoint sendRemoteEP = new IPEndPoint(IPAddress.Parse(ipSender), portSender);
                UdpClient sendUdpClient = new UdpClient(8888);
                
                sendUdpClient.Connect(sendRemoteEP);
                if (!String.IsNullOrEmpty(sFile))
                {
                    BackgroundWorker worker = new BackgroundWorker();
                    worker.WorkerReportsProgress = true;
                    worker.DoWork += delegate (object sender, DoWorkEventArgs e)
                    {
                        
                        //send starting message
                        while (sendData == true)
                        {
                            using (StreamReader sr = new StreamReader(sFile))
                            {
                                string line = sr.ReadLine();

                                while (line != null && sendData == true)
                                {
                                    if ((sw.Elapsed.TotalMilliseconds) >= timing)
                                    {
                                        object[] messageArray = line.Split(';');
                                        double currentTime = Double.Parse(messageArray[0].ToString());
                                        string oscAdress = (string)messageArray[1];
                                        object[] convertedMessage = new object[messageArray.Length - 2];

                                        for (int i = 2; i < messageArray.Length; i++)
                                        {
                                            convertedMessage[i - 2] = float.Parse(messageArray[i].ToString().Replace("|f", "").Replace(",", "."), CultureInfo.InvariantCulture);
                                        }
                                        var message = new OscMessage(oscAdress, convertedMessage);
                                        sendUdpClient.Send(message.GetBytes(), message.GetBytes().Length);
                                        line = sr.ReadLine();

                                        if(line != null)
                                        {
                                            object[] nextMessageArray = line.Split(';');
                                            double nextTime = Double.Parse(nextMessageArray[0].ToString());
                                            timing = nextTime - currentTime;
                                        }
                                       
                                        sw = Stopwatch.StartNew();
                                    }
                                }
                                
                            }                        
                        }
                    };

                    worker.RunWorkerCompleted += delegate (object sender, RunWorkerCompletedEventArgs e)
                    {
                        sendUdpClient.Close();
                    };
                    worker.RunWorkerAsync(10);
                }

                else
                {
                    WriteLine("Keine Datei ausgewählt");
                }
            }
            catch(Exception e)
            {
                WriteLine(e.Message);
            }
        }

        void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            WriteLine((string) e.UserState);
        }

        private void stopBtnSend_Click(object sender, RoutedEventArgs e)
        {
            WriteLine("Sending will be stopped...");
            sendData = false;
        }

        private void fileButton_Click(object sender, RoutedEventArgs e)
        {
            WinForms.OpenFileDialog fileDialog = new WinForms.OpenFileDialog();
            WinForms.DialogResult result = fileDialog.ShowDialog();

            if (result == WinForms.DialogResult.OK)
            {
                sFile = fileDialog.FileName;
                TextBlock tb = this.selectedFolderInfo;
                tb.Text = "";
                tb.Inlines.Add("Ausgewählte Datei: ");
                tb.Inlines.Add(new Run(sFile) { FontWeight = FontWeights.Bold });
            }
        }

       private void WriteLine(String line)
        {
            if(consoleBox.Visibility == Visibility.Visible)
            {
                Dispatcher.BeginInvoke((Action)(() => consoleBox.AppendText(line + "\n")));
                consoleBox.ScrollToEnd();
            }     
        }

        private void Write(String line)
        {
            if (consoleBox.Visibility == Visibility.Visible)
            {
                Dispatcher.BeginInvoke((Action)(() => consoleBox.AppendText(line)));
            }
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            consoleBox.Visibility = Visibility.Collapsed;
        }

        private void toggleConsole_Unchecked(object sender, RoutedEventArgs e)
        {
            consoleBox.Visibility = Visibility.Visible;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            int tmp = Int32.Parse(peopleNumber.Text)+1;
            if(tmp <= 3)
            {
                TextBox txtNumber = new TextBox();
                txtNumber.Name = "subscribeMessage" + tmp;
                txtNumber.Text = "/subscribe/unknown-" + tmp + "/maya/+ joint +/matrix";
                txtNumber.FontSize = 20;
                txtNumber.SetValue(Grid.RowProperty, tmp - 1);
                Thickness myThickness = new Thickness();
                myThickness.Bottom = 5;
                txtNumber.Margin = myThickness;
                subscribeMessageGrid.Children.Add(txtNumber);
                subscribeMessageGrid.RegisterName(txtNumber.Name, txtNumber);
                peopleCounter = tmp;
                peopleNumber.Text = Convert.ToString(Int32.Parse(peopleNumber.Text) + 1);
            }
            
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            int tmp = Int32.Parse(peopleNumber.Text);
            if(tmp > 1)
            {
                TextBox tb = (TextBox)this.subscribeMessageGrid.FindName("subscribeMessage" + tmp);
                subscribeMessageGrid.Children.Remove(tb);
                subscribeMessageGrid.UnregisterName("subscribeMessage" + tmp);
                peopleNumber.Text = Convert.ToString(tmp - 1);
            }
        }
    }
}
